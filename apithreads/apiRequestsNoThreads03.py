#!/usr/bin/python3
"""API requests with threads | rzfeeser@alta3.com"""

# standard library
from concurrent.futures import ThreadPoolExecutor, as_completed
from time import time

# python3 -m pip install requests
import requests


url_list = [
    "https://api.le-systeme-solaire.net/rest/bodies/lune",
    "https://api.le-systeme-solaire.net/rest/bodies/phobos",
    "https://api.le-systeme-solaire.net/rest/bodies/deimos",
    "https://api.le-systeme-solaire.net/rest/bodies/europe",
    "https://api.le-systeme-solaire.net/rest/bodies/callisto",
    "https://api.le-systeme-solaire.net/rest/bodies/himalia",
    "https://api.le-systeme-solaire.net/rest/bodies/elara",
    "https://api.le-systeme-solaire.net/rest/bodies/sinope",
    "https://api.le-systeme-solaire.net/rest/bodies/leda",
    "https://api.le-systeme-solaire.net/rest/bodies/thebe",
]

def download_file(url):
    resp = requests.get(url, stream=True)
    return resp.url, resp.elapsed
    # tuple --> ("http://whatever", "00:00:0.5645")
start = time()

processes = []

with ThreadPoolExecutor(max_workers=5) as executor:
    for url in url_list:
        processes.append(executor.submit(download_file, url)) 

for task in as_completed(processes):   
    url, elapsed= task.result()
    print(f""" 
    URL: {url}
    Send/Arrival Time Elapsed: {elapsed}""")

print(f'Total time taken: {time() - start}')
